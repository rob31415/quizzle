# meta info

autor: robert

initial date: june '21
last mod: march '23

# purpose

this is a demo project to showcase knowledge of the following topics:

- jakarta EE 8 technology
    - Servlets 4.0
    - Security API
    - JSF 2.3
    - JPA 2.2
    - CDI 2.0 / DI
    - JAX RS 2.1
    - JSON P 1.1 / JSON B 1.0
    - WebSockets 1.1
- JBoss wildfly application server
- API specification
    - OpenAPI (swagger)
- writing software requirements specification
    - use cases
    - navigation map
    - UML
- writing Architecture Decision Records
- virtualization
    - vagrant
    - docker
- web tech
    - angular

# development environment

## prerequisites

- linux OS (host system; laptop/desktop hardware PC)
- virtualbox
- vagrant
- git (optional)

## overview

suggested set up:

1. get a vagrant box which has docker installed
2. inside that, create and start a "development container" (JakartaEE) and a database (mariadb)

Note: All of this is provided with the sourcecode repository for simple usage (see initial setup below).

The development-container is derived from an official wildfly container, adds maven and the ability to go inside it (i.e. to use bash).

A sourcecode directory on the host-system is made available through the VM to the docker container.
Ports of the wildfly server are made available from the docker container, through the VM to the host system.

So in the end, codeeditors & browsers on the hostsystem can be utilized, while keeping details of a buildsystem seperated from the hostsystem.

## devenv initial setup

This has to be done initially one time.

    - git clone https://gitlab.com/rob31415/quizzle.git
    - cd quizzle/devops/vagrant
    - vagrant plugin install vagrant-docker-compose
    - vagrant up                    # go and grab a coffee...
    - vagrant ssh                   # now inside the VM
    - cd /host/devops/docker/build
    - ./buiĺd_images.sh             # continue your coffee

## note: initial project structure

This was done initially to create an empty project structure.

    mvn archetype:generate  -DarchetypeGroupId=org.wildfly.archetype -DarchetypeArtifactId=wildfly-jakartaee-ear-archetype -DgroupId=com.omnicorp -DartifactId=quizzle -DinteractiveMode=false

## start devenv

This has to be done every time before starting to write code.

    - cd SOME-BASE-DIR/quizzle/devops/vagrant
    - vagrant up && vagrant ssh                 # now inside the VM
    - /host/devops/docker/startup/startup-container.sh

Now, inside the container, the sources can be built and deployed:

    - cd /host      # equals  VM:/host/src  and  HOST:SOME-BASE-DIR/quizzle/src

suggested work cycle:

    - # modify code
    - mvn clean && mvn install wildfly:deploy

## after devenv start

### accessing the app

    http://localhost:8080/quizzle-web/
    creds: a/b

### accessing app server management web console

    http://localhost:9990/console/index.html
    creds: admin/admin

### setup wildfly initially

- go to management console, deployments and deploy mariadb-client-jdbc.jar (quizzle-ear/target/quizzle/lib/mariadb-java-client.jar)
- go to subsystems, datasource & drivers, add datasource - jdbc:mariadb://db:3306/quizzle - root/root
- deploy the app one time ("cd /host/ && ./build.sh" in container named "as" - as in "Application Server") - it'll fail, because no tables yet - but .sql files containing DDL will be created (quizzle-ejb/target/generated-sources/dbscripts/)
- apply contents of file "create-ddl.sql" on the db
- deploy the app one more time, should work now
- for DB content, you can use Playground.java - after DDLs ran successfully, uncomment "create.run();" for example - and re-deploy. This populates the DB with some example data
