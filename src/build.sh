#!/usr/bin/env bash

# if there is any argument, do a clean before the build
if [[ "$#" -gt 0 ]]
then
    mvn clean
fi
mvn install wildfly:deploy -Dhttps.protocols=TLSv1.2
