package com.omnicorp.dao;

import java.util.*;
import java.util.logging.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import javax.transaction.UserTransaction;
import javax.annotation.Resource;
import javax.ejb.*;

import com.omnicorp.model.*;

@Singleton
//@Local
//@Startup
//@Stateless
//@Transactional
@TransactionManagement(javax.ejb.TransactionManagementType.BEAN)
public class QuizDao {
    private static Logger LOGGER = Logger.getLogger(QuizDao.class.getName());

    @PersistenceContext
    private EntityManager em;    
    @Resource
    private UserTransaction ut;

    //private QuizDao() {}

    public List<Quiz> getQuizzes() {
        if(em==null) {
            LOGGER.log(Level.WARNING, "entity manager is null");
        }
        if(Quiz.class==null){
            LOGGER.log(Level.WARNING, "Quiz.class is null");
        }

        Query q = em.createQuery("select q from quiz q", Quiz.class);
        return q.getResultList();
    }
    
    public Quiz getQuiz(Long id) {
        Query q = em.createQuery("select q from quiz q where q.id="+id, Quiz.class);
        Quiz quiz = (Quiz)(q.getSingleResult());
/*
        LOGGER.log(Level.FINE, "quiz id:" + id);
        LOGGER.log(Level.FINE, "#Questions deserialized:" + quiz.getQuestions().size());
        LOGGER.log(Level.FINE, "1st Question text:" + quiz.getQuestions().get(0).getText());
        LOGGER.log(Level.FINE, "multichoice answer id:" + quiz.getQuestions().get(0).getMultipleChoiceAnswer().getId());
        LOGGER.log(Level.FINE, "1st multichoice text: " + quiz.getQuestions().get(0).getMultipleChoiceAnswer().getChoices().get(0).getText());
*/
        return quiz;
    }

/*    public List<Question> getQuestions(Quiz quiz) {
        Query q = em.createQuery("select q from question q where q.quiz=:qui", Question.class).setParameter("qui",quiz);
        return q.getResultList();
    }
*/
    public void addQuiz(InlineObject body) {
        /*
        var quiz = new Quiz();
        var question = new Question();
        quiz.getQuestions().add(question);
        question.setQuiz(quiz);
        
        quiz.setName(body.getName());
        quiz.setDescription(body.getDescription());
        quiz.setUserId(0L);
        
        question.setText("Whats up!?");

        String res="nothing";
        try {
            ut.begin();
            em.persist(quiz);
            em.persist(question);
            ut.commit();
            res = (quiz.getId() == null) ? "bad" : "good";
        } catch (Exception e) {
            System.out.println("OOPS " + e);
        }
        */
    }

}