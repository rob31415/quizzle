package com.omnicorp.jsf;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;

import com.omnicorp.model.*;

import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import com.omnicorp.jsonToModelConv.QuizConverter;
import com.omnicorp.dao.QuizDao;
import com.omnicorp.business.Ranking;


@Named("takeQuizBean")
// TODO: later, when transition to/from comments is implemented, make this ConversationScoped
//@javax.enterprise.context.SessionScoped
@ViewScoped
public class TakeQuizBean implements Serializable {
    private static Logger LOGGER = Logger.getLogger(TakeQuizBean.class.getName());
    @EJB Ranking ranking;

    static private class State {
        private static List<Question> questions;
        private static List<List<MultipleChoiceItem>> selectedAsCorrect; // the user selected these as being correct
        private static int currentQuestionIndex;
        private static Long selectedQuizId;

        static {init();}

        private static void init() {
            questions = null;
            selectedAsCorrect=null;
            currentQuestionIndex=-1;
            selectedQuizId = -1L;
        }

        static void store() {
            getSession().setAttribute("takeQuiz.questions",questions);
            getSession().setAttribute("takeQuiz.currentQuestionIndex",currentQuestionIndex);
            getSession().setAttribute("takeQuiz.selectedAsCorrect",selectedAsCorrect);
        }

        static void retrieve() {
                List<Question> questionsFromSession = (List<Question>)getSession().getAttribute("takeQuiz.questions");
            if(questionsFromSession!=null) {questions=questionsFromSession;}

            Integer indexFromSession = (Integer)getSession().getAttribute("takeQuiz.currentQuestionIndex");
            if(indexFromSession!=null) {currentQuestionIndex=indexFromSession;}
    
            var selectedAsCorrectFromSession = (List<List<MultipleChoiceItem>>)getSession().getAttribute("takeQuiz.selectedAsCorrect");
            if(selectedAsCorrectFromSession!=null) {selectedAsCorrect=selectedAsCorrectFromSession;}
    
            selectedQuizId = (Long)getSession().getAttribute("selectQuiz.selectedQuizId");
        }

        static void destroy() {
            getSession().removeAttribute("takeQuiz.questions");
            getSession().removeAttribute("takeQuiz.currentQuestionIndex");
            getSession().removeAttribute("takeQuiz.selectedAsCorrect");
            init();
        }

        private static HttpSession getSession() {
            FacesContext facesContext = FacesContext.getCurrentInstance();
            return (HttpSession) facesContext.getExternalContext().getSession(false);
        }
    }


    @PostConstruct
    public void init() {
        LOGGER.log(Level.FINE, "TakeQuizBean.init");
        State.retrieve();
        if (State.currentQuestionIndex == -1) {
            State.questions = QuizConverter.getQuizById(State.selectedQuizId).getQuestions();
            if (State.questions != null && State.questions.size() > 0) {
                State.selectedAsCorrect = new ArrayList<>(State.questions.size());
                for(int i=0;i<State.questions.size();i++) {State.selectedAsCorrect.add(i,new ArrayList<MultipleChoiceItem>());}
                State.currentQuestionIndex = 0;
            } else {
                LOGGER.log(Level.WARNING, "No questions in this quiz");
            }
        } else {
            LOGGER.log(Level.FINE, "Data for quiz-taking retrieved from session");
        }
    }

    public String next() {
        if (State.questions.get(State.currentQuestionIndex) == null) {
            LOGGER.log(Level.WARNING, "No current question");
        } else {
            if(State.currentQuestionIndex<State.questions.size()-1) {
                State.currentQuestionIndex++;
                State.store();
            }
        }
        return "takeQuiz?faces-redirect=true";
    }

    public String previous()  {
        if (State.questions.get(State.currentQuestionIndex) == null) {
            LOGGER.log(Level.WARNING, "No current question");
        } else {
            if(State.currentQuestionIndex>0) {
                State.currentQuestionIndex--;
                State.store();
            }
        }
        return "takeQuiz?faces-redirect=true";
    }

    public String finito() {
        // TODO: check and warn if not all are answered
        for(int i=0;i<State.questions.size();i++) {
            State.selectedAsCorrect.get(i).stream().forEach(e->LOGGER.log(Level.FINE, e.getText()));
        }
        //List<Question> questions;     //TODO get questions of quiz from quizdao
        
        //validate(questions, State.questions);

        State.destroy();
        return "index";
    }

    public String giveUp() {
        State.destroy();
        return "index";
    }

    public String getCurrentQuestionText() {
        if (State.currentQuestionIndex > -1) {
            return State.questions.get(State.currentQuestionIndex).getText();
        }
        return "";
    }

    public List<MultipleChoiceItem> getAnswers() {
        return State.questions.get(State.currentQuestionIndex).getMultipleChoiceAnswer().getChoices();
    }

    public List<MultipleChoiceItem> getSelectedAsCorrect() {
        return State.selectedAsCorrect.get(State.currentQuestionIndex);
    }

    public void setSelectedAsCorrect(List<MultipleChoiceItem> selectedAsCorrect) {
        State.selectedAsCorrect.set(State.currentQuestionIndex, selectedAsCorrect);
    }

}


/*
datamodel:
    MultipleChoiceAnswer
        MultipleChoiceItem
            text
            isCorrect  
https://stackoverflow.com/questions/3694450/how-can-i-show-hide-component-with-jsf
*/