package com.omnicorp.jsf;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import java.util.logging.*;

import com.omnicorp.jsonToModelConv.*;
import com.omnicorp.model.MultipleChoiceItem;
import com.omnicorp.jsf.TakeQuizBean;

@FacesConverter(value="mciFacesConverter", forClass=MultipleChoiceItem.class)
public class MciFacesConverter implements Converter {

    private static Logger LOGGER = Logger.getLogger(MciFacesConverter.class.getName());

    // parseInt returns an int primitive while valueOf returns an Integer object.
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        //LOGGER.log(Level.FINE, "value=" + value);
        //Long valueCasted = Long.valueOf(value);
        //return QuizConverter.getMultipleChoiceItemById(valueCasted);

        //var m = context.getExternalContext().getApplicationMap();
        //LOGGER.log(Level.FINE, "blag="+m);

        // FacesContext.getCurrentInstance()
        //TakeQuizBean b = (TakeQuizBean) context.getExternalContext().getApplicationMap().get("takeQuizBean");
        //if(b==null) {
        //    LOGGER.log(Level.WARNING , "bean is null. value=" + value);
        //}
        //return b.getCurrentChoices();

        var r = new MultipleChoiceItem();
        r.setId(9L);
        r.setText("John");
        return r;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        return  ((MultipleChoiceItem) value).getId().toString();
    }
}
