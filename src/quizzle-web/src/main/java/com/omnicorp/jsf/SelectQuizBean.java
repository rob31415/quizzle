package com.omnicorp.jsf;

import java.io.IOException;
import java.io.Serializable;
import java.util.Base64;
import java.util.List;

import javax.inject.Named;
import javax.servlet.http.HttpSession;
import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Startup;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.type.TypeFactory;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.*;
import javax.faces.context.FacesContext;

import com.omnicorp.model.Quiz;

import com.omnicorp.jsonToModelConv.QuizConverter;


@Named("selectQuizBean")
@SessionScoped
public class SelectQuizBean implements Serializable {
    private String userName = "Guest";
    private List<Quiz> quizzes;
    private Quiz currentSelection;
    private String difficulty;
    private float sizePercent;
    private int size;


    @PostConstruct
    public void init() {
        quizzes = QuizConverter.getQuizzes();

        if(quizzes.size()>0) {
            currentSelection = quizzes.get(0);
        }
        sizePercent = 100.0f;
        size = 100;
        difficulty = "1";
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public List<Quiz> getQuizzes() {
        return quizzes;
    }

    public void setQuizzes(List<Quiz> quizzes) {
        this.quizzes = quizzes;
    }

    public Quiz getCurrentSelection() {
        return currentSelection;
    }

    public void setCurrentSelection(Quiz currentSelection) {
        this.currentSelection = currentSelection;
    }

    public String getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(String difficulty) {
        this.difficulty = difficulty;
    }

    public float getSizePercent() {
        return sizePercent;
    }

    public void setSizePercent(float sizePercent) {
        this.sizePercent = sizePercent;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public void validateQuizSelection(FacesContext context, UIComponent comp, Object value) {
        if (value == null) {
            ((UIInput) comp).setValid(false);
            FacesMessage message = new FacesMessage("No Quiz Selected");
            context.addMessage(comp.getClientId(context), message);
        }
    }

    public void select() throws IOException {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
        session.setAttribute("selectQuiz.selectedQuizId", currentSelection.getId());
        facesContext.getExternalContext().redirect("takeQuiz.xhtml");
    }

}
