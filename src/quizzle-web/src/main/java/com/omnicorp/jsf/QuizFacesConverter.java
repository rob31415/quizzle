package com.omnicorp.jsf;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

import com.omnicorp.jsonToModelConv.*;
import com.omnicorp.model.Quiz;

@FacesConverter(value="quizFacesConverter", forClass=Quiz.class)
public class QuizFacesConverter implements Converter {

    // parseInt returns an int primitive while valueOf returns an Integer object.
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        Long valueCasted = Long.valueOf(value);
        return QuizConverter.getQuizById(valueCasted);

        // TODO: get this to work, don't do a server request just because of this
        //SelectQuizBean b = (SelectQuizBean) FacesContext.getCurrentInstance().
        //    getExternalContext().getApplicationMap().get("selectQuizBean");
        //return b.getCurrentSelection();
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        return  ((Quiz) value).getId().toString();
    }
}
