package com.omnicorp.api;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.omnicorp.model.*;
import com.omnicorp.dao.*;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

import io.swagger.annotations.*;

import java.io.InputStream;
import java.util.Map;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;
import javax.ejb.EJB;
import javax.validation.Valid;


abstract class MixinForQuiz {
    @JsonIgnoreProperties({ "questions" }) abstract class IgnoreQuestions {}
    @JsonManagedReference  List<MultipleChoiceItem> questions;      // QuizQuestionRel_OneSide
}
abstract class MixinForQuestion {
    @JsonBackReference Quiz quiz;                                           // QuizQuestionRel_NSide
    @JsonManagedReference  MultipleChoiceAnswer multipleChoiceAnswer;       // QuestionAnswerRel_OneSide
}
abstract class MixinForMultipleChoiceAnswer {
    @JsonManagedReference  List<MultipleChoiceItem> choices;    // AnswerItemRel_OneSide
    @JsonBackReference Question question;                       // QuestionAnswerRel_OtherSide
}
abstract class MixinForMultipleChoiceItem {
    @JsonBackReference MultipleChoiceAnswer multipleChoiceAnswer;   // AnswerItemRel_NSide
}


public class QuizApiImpl implements QuizApi{
    @EJB QuizDao quizDao;

    public Response addQuiz(@Valid @NotNull InlineObject body) {
        quizDao.addQuiz(body);
        //return Response.ok().entity(res).build();
        return null;
    }

    @DELETE
    @Path("/{id}")
    @ApiOperation(value = "Delete a Quiz", notes = "Deep (deletes also all questions and answers of that quiz).", response = Void.class, authorizations = {
        @Authorization(value = "quizzle_auth", scopes = {
            @AuthorizationScope(scope = "write:quiz", description = "modify quizzes belonging to your account"),
            @AuthorizationScope(scope = "read:quiz", description = "read quizzes belonging to your account") })
         }, tags={ "quiz" })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "ok", response = Void.class),
        @ApiResponse(code = 404, message = "Quiz not found", response = Void.class)
    })
    public Response deleteQuiz(@PathParam("id") @ApiParam("Quiz id to delete") Long id) {
        return Response.ok().entity("magic!").build();
    }

    @GET
    @Path("/{id}")
    @Produces({ "application/json" })
    @ApiOperation(value = "List all Questions and Answers of a Quiz", notes = "Deep. All details of Quiz are retrieved.", response = Question.class, responseContainer = "List", authorizations = {
        @Authorization(value = "quizzle_auth", scopes = {
            @AuthorizationScope(scope = "write:quiz", description = "modify quizzes belonging to your account"),
            @AuthorizationScope(scope = "read:quiz", description = "read quizzes belonging to your account") })
         }, tags={ "quiz" })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "successful operation", response = Question.class, responseContainer = "List"),
        @ApiResponse(code = 400, message = "Invalid id", response = Void.class)
    })
    public Response getQuiz(@PathParam("id") @ApiParam("Id of quiz") Long id) {
        var quiz = quizDao.getQuiz(id);

        var mapper = new ObjectMapper();
        mapper.setSerializationInclusion(Include.NON_NULL);
        mapper.setSerializationInclusion(Include.NON_EMPTY);
        mapper.addMixIn(Quiz.class,                 MixinForQuiz.class);
        mapper.addMixIn(Question.class,             MixinForQuestion.class);
        mapper.addMixIn(MultipleChoiceAnswer.class, MixinForMultipleChoiceAnswer.class);
        mapper.addMixIn(MultipleChoiceItem.class,   MixinForMultipleChoiceItem.class);

        try {
            return Response.ok().entity(mapper.writeValueAsString(quiz)).build();
        } catch (JsonProcessingException e) {
            return Response.ok().entity(e).build();
        }
    }

    public Response getQuizzes() {
        var object = quizDao.getQuizzes();
        var mapper = new ObjectMapper();
        mapper.setSerializationInclusion(Include.NON_NULL);
        mapper.setSerializationInclusion(Include.NON_EMPTY);
        mapper.addMixIn(Quiz.class, MixinForQuiz.IgnoreQuestions.class);        // also avoids cyclic refs/stackoverflow issue
        try {
            return Response.ok().entity(mapper.writeValueAsString(object)).build();
        } catch (JsonProcessingException e) {
            return Response.ok().entity(e).build();
        }
    }

    @PUT
    @Path("/{id}")
    @Consumes({ "application/json" })
    @ApiOperation(value = "Update an existing quiz", notes = "", response = Void.class, authorizations = {
        @Authorization(value = "quizzle_auth", scopes = {
            @AuthorizationScope(scope = "write:quiz", description = "modify quizzes belonging to your account"),
            @AuthorizationScope(scope = "read:quiz", description = "read quizzes belonging to your account") })
         }, tags={ "quiz" })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "ok", response = Void.class),
        @ApiResponse(code = 403, message = "Quizname already exists", response = Void.class),
        @ApiResponse(code = 404, message = "Quiz not found", response = Void.class)
    })
    public Response updateQuiz(@PathParam("id") @ApiParam("Id of quiz") Long id,@Valid @NotNull InlineObject1 body) {
        return Response.ok().entity("magic!").build();
    }
}
