package com.omnicorp.api;

import com.omnicorp.model.Message;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

import io.swagger.annotations.*;

import java.io.InputStream;
import java.util.Map;
import java.util.List;
import javax.validation.constraints.*;
import javax.validation.Valid;

@Path("/message")
@Api(description = "the message API")
public class MessageApiImpl implements MessageApi {

    @POST
    @ApiOperation(value = "Create a message exchanged between two users", notes = "", response = Void.class, authorizations = {
        @Authorization(value = "quizzle_auth", scopes = {
            @AuthorizationScope(scope = "write:quiz", description = "modify quizzes belonging to your account"),
            @AuthorizationScope(scope = "read:quiz", description = "read quizzes belonging to your account") })
         }, tags={ "message" })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "successful operation", response = Void.class)
    })
    public Response createMessage(@Valid @NotNull Message body) {
        return Response.ok().entity("magic!").build();
    }

    @GET
    @Path("/{userId}")
    @Produces({ "application/json" })
    @ApiOperation(value = "List all Messages for or to a given user", notes = "", response = Message.class, responseContainer = "List", authorizations = {
        @Authorization(value = "quizzle_auth", scopes = {
            @AuthorizationScope(scope = "write:quiz", description = "modify quizzes belonging to your account"),
            @AuthorizationScope(scope = "read:quiz", description = "read quizzes belonging to your account") })
         }, tags={ "message" })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "successful operation", response = Message.class, responseContainer = "List"),
        @ApiResponse(code = 400, message = "Invalid id", response = Void.class)
    })
    public Response getMessages(@PathParam("userId") @ApiParam("Id of user") Long userId) {
        return Response.ok().entity("magic!").build();
    }
}
