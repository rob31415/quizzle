package com.omnicorp.api;

import com.omnicorp.model.MultipleChoiceAnswer;
import com.omnicorp.model.TextAnswer;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

import io.swagger.annotations.*;

import java.io.InputStream;
import java.util.Map;
import java.util.List;
import javax.validation.constraints.*;
import javax.validation.Valid;

public class AnswerApiImpl implements AnswerApi {

    public Response createMultipleChoiceAnswer(@Valid @NotNull MultipleChoiceAnswer body) {
        return Response.ok().entity("magic!").build();
    }

    public Response createTextualAnswer(@Valid @NotNull TextAnswer body) {
        return Response.ok().entity("magic!").build();
    }

    public Response deleteAnswer(@PathParam("id") @ApiParam("Answer id to delete") Long id) {
        return Response.ok().entity("magic!").build();
    }

    public Response updateMultipleChoiceAnswer(@PathParam("id") @ApiParam("Id of answer") Long id,@Valid @NotNull MultipleChoiceAnswer body) {
        return Response.ok().entity("magic!").build();
    }

    public Response updateTextualAnswer(@PathParam("id") @ApiParam("Id of question") Long id,@Valid @NotNull TextAnswer body) {
        return Response.ok().entity("magic!").build();
    }
}
