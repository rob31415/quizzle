package com.omnicorp.api;

import com.omnicorp.model.QuizSessionHistory;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

import io.swagger.annotations.*;

import java.io.InputStream;
import java.util.Map;
import java.util.List;
import javax.validation.constraints.*;
import javax.validation.Valid;

@Path("/quizSessionHistory")
@Api(description = "the quizSessionHistory API")
public class QuizSessionHistoryApiImpl implements QuizSessionHistoryApi {

    @GET
    @Path("/byQuiz/{id}")
    @Produces({ "application/json" })
    @ApiOperation(value = "List all quiz taking history for a given quiz", notes = "", response = QuizSessionHistory.class, responseContainer = "List", authorizations = {
        @Authorization(value = "quizzle_auth", scopes = {
            @AuthorizationScope(scope = "write:quiz", description = "modify quizzes belonging to your account"),
            @AuthorizationScope(scope = "read:quiz", description = "read quizzes belonging to your account") })
         }, tags={ "quizSessionHistory" })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "ok", response = QuizSessionHistory.class, responseContainer = "List"),
        @ApiResponse(code = 400, message = "Invalid id", response = Void.class)
    })
    public Response getQuizSessionHistoryByQuiz(@PathParam("id") @ApiParam("Id of user") Long id) {
        return Response.ok().entity("magic!").build();
    }

    @GET
    @Path("/byUser/{id}")
    @Produces({ "application/json" })
    @ApiOperation(value = "List all quiz taking history for a given user", notes = "", response = QuizSessionHistory.class, responseContainer = "List", authorizations = {
        @Authorization(value = "quizzle_auth", scopes = {
            @AuthorizationScope(scope = "write:quiz", description = "modify quizzes belonging to your account"),
            @AuthorizationScope(scope = "read:quiz", description = "read quizzes belonging to your account") })
         }, tags={ "quizSessionHistory" })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "ok", response = QuizSessionHistory.class, responseContainer = "List"),
        @ApiResponse(code = 400, message = "Invalid id", response = Void.class)
    })
    public Response getQuizSessionHistoryByUser(@PathParam("id") @ApiParam("Id of user") Long id) {
        return Response.ok().entity("magic!").build();
    }
}
