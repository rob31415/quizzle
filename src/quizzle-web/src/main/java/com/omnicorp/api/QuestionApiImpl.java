package com.omnicorp.api;

import com.omnicorp.model.InlineObject2;
import com.omnicorp.model.InlineObject3;
import com.omnicorp.model.Question;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

import io.swagger.annotations.*;

import java.io.InputStream;
import java.util.Map;
import java.util.List;
import javax.validation.constraints.*;
import javax.validation.Valid;

@Path("/question")
@Api(description = "the question API")
public class QuestionApiImpl implements QuestionApi {

    @GET
    @Path("/{id}")
    @Produces({ "application/json" })
    @ApiOperation(value = "Get a certain Question and it's Answers", notes = "Deep.", response = Question.class, authorizations = {
        @Authorization(value = "quizzle_auth", scopes = {
            @AuthorizationScope(scope = "write:quiz", description = "modify quizzes belonging to your account"),
            @AuthorizationScope(scope = "read:quiz", description = "read quizzes belonging to your account")
        })
    }, tags={ "question",  })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "successful operation", response = Question.class),
        @ApiResponse(code = 400, message = "Invalid id", response = Void.class)
    })
    public Response getQuestion(@PathParam("id") @ApiParam("Id of question") Long id) {
        return Response.ok().entity("magic!").build();
    }

    @POST
    @ApiOperation(value = "Create question", notes = "This can only be done by a logged in user.", response = Void.class, authorizations = {
        @Authorization(value = "quizzle_auth", scopes = {
            @AuthorizationScope(scope = "write:quiz", description = "modify quizzes belonging to your account"),
            @AuthorizationScope(scope = "read:quiz", description = "read quizzes belonging to your account") })
         }, tags={ "question" })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "ok", response = Void.class),
        @ApiResponse(code = 404, message = "Quiz not found", response = Void.class)
    })
    public Response createQuestion(@Valid @NotNull InlineObject2 body) {
        return Response.ok().entity("magic!").build();
    }

    @DELETE
    @Path("/{id}")
    @ApiOperation(value = "Delete a Question", notes = "", response = Void.class, authorizations = {
        @Authorization(value = "quizzle_auth", scopes = {
            @AuthorizationScope(scope = "write:quiz", description = "modify quizzes belonging to your account"),
            @AuthorizationScope(scope = "read:quiz", description = "read quizzes belonging to your account") })
         }, tags={ "question" })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "ok", response = Void.class),
        @ApiResponse(code = 404, message = "Question not found", response = Void.class)
    })
    public Response deleteQuestion(@PathParam("id") @ApiParam("Question id to delete") Long id) {
        return Response.ok().entity("magic!").build();
    }

    @PUT
    @Path("/{id}")
    @Consumes({ "application/json" })
    @ApiOperation(value = "Update an existing question", notes = "", response = Void.class, authorizations = {
        @Authorization(value = "quizzle_auth", scopes = {
            @AuthorizationScope(scope = "write:quiz", description = "modify quizzes belonging to your account"),
            @AuthorizationScope(scope = "read:quiz", description = "read quizzes belonging to your account") })
         }, tags={ "question" })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "ok", response = Void.class),
        @ApiResponse(code = 404, message = "Question not found", response = Void.class)
    })
    public Response updateQuestion(@PathParam("id") @ApiParam("Id of question") Long id,@Valid @NotNull InlineObject3 body) {
        return Response.ok().entity("magic!").build();
    }
}
