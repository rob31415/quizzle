package com.omnicorp.api;

import com.omnicorp.model.InlineObject4;
import com.omnicorp.model.QuizSession;
import com.omnicorp.model.QuizSession1;
import com.omnicorp.model.QuizSessionResult;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

import io.swagger.annotations.*;

import java.io.InputStream;
import java.util.Map;
import java.util.List;
import javax.validation.constraints.*;
import javax.validation.Valid;

@Path("/quizSession")
@Api(description = "the quizSession API")
public class QuizSessionApiImpl implements QuizSessionApi {

    @POST
    @Path("/{id}")
    @ApiOperation(value = "Answer (given by someone taking a quiz) to a question of a quiz", notes = "This expects either \"textAnswer\" or elements in \"choices\", not both. Regarding a multiple-choice answer - the server assumes for all choices \"correct=false\", so only true ones have to be given in \"choices\".", response = Void.class, authorizations = {
        @Authorization(value = "quizzle_auth", scopes = {
            @AuthorizationScope(scope = "write:quiz", description = "modify quizzes belonging to your account"),
            @AuthorizationScope(scope = "read:quiz", description = "read quizzes belonging to your account") })
         }, tags={ "quizSession" })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Question answered correctly", response = Void.class),
        @ApiResponse(code = 405, message = "Question answered incorrectly", response = Void.class)
    })
    public Response answerQuestion(@PathParam("id") @ApiParam("Id of a quiz session") Long id,@Valid InlineObject4 multipleChoiceAnswer) {
        return Response.ok().entity("magic!").build();
    }

    @POST
    @Produces({ "application/json" })
    @ApiOperation(value = "Create a quiz session for starting to take a quiz", notes = "", response = QuizSession1.class, authorizations = {
        @Authorization(value = "quizzle_auth", scopes = {
            @AuthorizationScope(scope = "write:quiz", description = "modify quizzes belonging to your account"),
            @AuthorizationScope(scope = "read:quiz", description = "read quizzes belonging to your account") })
         }, tags={ "quizSession" })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Successful response", response = QuizSession1.class),
        @ApiResponse(code = 405, message = "Computer says No", response = Void.class)
    })
    public Response createQuizSession(@Valid @NotNull QuizSession body) {
        return Response.ok().entity("magic!").build();
    }

    @DELETE
    @Path("/{id}")
    @Produces({ "application/json" })
    @ApiOperation(value = "End a quiz", notes = "If all questions were answered, quizSessionHistory is being updated and 200 is returned. If not, 400. An inactive quizSession will be cleaned up automatically after a certain, reasonable amount of time.", response = QuizSessionResult.class, tags={ "quizSession" })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Quiz completed", response = QuizSessionResult.class),
        @ApiResponse(code = 400, message = "Quiz not completed", response = Void.class),
        @ApiResponse(code = 404, message = "No such quiz session", response = Void.class)
    })
    public Response deleteQuizSession(@PathParam("id") @ApiParam("The id of the quiz session to end") String id) {
        return Response.ok().entity("magic!").build();
    }
}
