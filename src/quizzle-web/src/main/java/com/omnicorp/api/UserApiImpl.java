package com.omnicorp.api;

import com.omnicorp.model.Credentials;
import com.omnicorp.model.Id;
import com.omnicorp.model.Password;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

import io.swagger.annotations.*;

import java.io.InputStream;
import java.util.Map;
import java.util.List;
import javax.validation.constraints.*;
import javax.validation.Valid;

@Path("/user")
@Api(description = "the user API")
public class UserApiImpl implements UserApi {

    @POST
    @Produces({ "application/json" })
    @ApiOperation(value = "Create user", notes = "", response = Id.class, tags={ "user" })
    @ApiResponses(value = { 
        @ApiResponse(code = 201, message = "created", response = Id.class),
        @ApiResponse(code = 403, message = "name already exists", response = Void.class)
    })
    public Response createUser(@Valid @NotNull Credentials body) {
        return Response.ok().entity("magic!").build();
    }

    @DELETE
    @Path("/{username}")
    @ApiOperation(value = "Delete user", notes = "This can only be done by the logged in user.", response = Void.class, tags={ "user" })
    @ApiResponses(value = { 
        @ApiResponse(code = 400, message = "Invalid username supplied", response = Void.class),
        @ApiResponse(code = 404, message = "User not found", response = Void.class)
    })
    public Response deleteUser(@PathParam("username") @ApiParam("The name that needs to be deleted") String username) {
        return Response.ok().entity("magic!").build();
    }

    @GET
    @Path("/{username}")
    @Produces({ "application/json" })
    @ApiOperation(value = "Get user id by name", notes = "", response = Id.class, tags={ "user" })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "successful operation", response = Id.class),
        @ApiResponse(code = 404, message = "User not found", response = Void.class)
    })
    public Response getUserByName(@PathParam("username") @ApiParam("The username who&#39;s id needs to be fetched.") String username) {
        return Response.ok().entity("magic!").build();
    }

    @POST
    @Path("/login")
    @ApiOperation(value = "Logs user in to the system", notes = "", response = Void.class, tags={ "user" })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "ok", response = Void.class),
        @ApiResponse(code = 401, message = "Invalid username/password supplied", response = Void.class)
    })
    public Response loginUser(@Valid @NotNull Credentials body) {
        return Response.ok().entity("magic!").build();
    }

    @GET
    @Path("/logout")
    @ApiOperation(value = "Logs out current user and deletes their session", notes = "", response = Void.class, tags={ "user" })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "ok", response = Void.class),
        @ApiResponse(code = 401, message = "no session to log out from", response = Void.class)
    })
    public Response logoutUser() {
        return Response.ok().entity("magic!").build();
    }

    @PUT
    @Path("/{username}")
    @ApiOperation(value = "User to be updated", notes = "This can only be done by the logged in user.", response = Void.class, tags={ "user" })
    @ApiResponses(value = { 
        @ApiResponse(code = 400, message = "Invalid user supplied", response = Void.class),
        @ApiResponse(code = 404, message = "User not found", response = Void.class)
    })
    public Response updateUser(@PathParam("username") @ApiParam("name that need to be updated") String username,@Valid @NotNull Password body) {
        return Response.ok().entity("magic!").build();
    }
}
