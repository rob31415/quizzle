package com.omnicorp.jsonToModelConv;

import java.io.Serializable;
import java.util.Base64;
import java.util.*;

import javax.inject.Named;
import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.annotation.PostConstruct;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.type.TypeFactory;

import com.omnicorp.model.*;

import java.util.logging.Level;
import java.util.logging.Logger;

public class QuizConverter {

    private static Logger LOGGER = Logger.getLogger(QuizConverter.class.getName());
    private static String ApiBaseUrl = "http://0.0.0.0:8080/quizzle-web/api/";     //TODO: configuration
    //private static String Credentials ="a:b";       // TODO: do it better obviously
    private static ObjectMapper objectMapper = new ObjectMapper();
    private static TypeFactory typeFactory = objectMapper.getTypeFactory();

    private static String getData(String endpoint) {
        //String encodedCreds = Base64.getEncoder().encodeToString(Credentials.getBytes());
        Client client = ClientBuilder.newClient();
        WebTarget webTarget = client.target(ApiBaseUrl + endpoint);
        //String jsonString = webTarget.request(MediaType.APPLICATION_JSON).header("Authorization", "Basic " + encodedCreds).get(String.class);
        String jsonString = webTarget.request(MediaType.APPLICATION_JSON).get(String.class);
        LOGGER.log(Level.FINE, jsonString);
        client.close();
        return jsonString;
    }

    public static List<Quiz> getQuizzes() {
        try {
            return objectMapper.readValue(getData("quiz"), typeFactory.constructCollectionType(List.class, Quiz.class));
        } catch (JsonProcessingException e) {
            LOGGER.log(Level.SEVERE, e.toString());
        }
        return new ArrayList<Quiz>();
    }

    public static Quiz getQuizById(Long id) {
        try {
            return objectMapper.readValue(getData("quiz/"+id), Quiz.class);
        } catch (JsonProcessingException e) {
            LOGGER.log(Level.SEVERE, e.toString());
        }
        return new Quiz();
    }

    public static MultipleChoiceItem getMultipleChoiceItemById(Long id) {
        /*
        try {
            Quiz q = getQuizById(id);
            return objectMapper.readValue(getData("quiz/"+id), Quiz.class);
        } catch (JsonProcessingException e) {
            LOGGER.log(Level.SEVERE, e.toString());
        }
        */
        return new MultipleChoiceItem();
    }
}