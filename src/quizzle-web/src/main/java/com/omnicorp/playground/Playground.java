package com.omnicorp.playground;

import javax.ejb.*;
import java.util.*;
import javax.annotation.*;
import java.util.logging.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.UserTransaction;

import com.omnicorp.model.*;

/*
When working (starting to work) on (de)serializing models, you might sometimes want
something to "test"/play around against a real database.
Unit tests are not meant for this, so this here is supposed to provide such kind
of a playground for developers.
*/

@Startup
@Singleton
@TransactionManagement(javax.ejb.TransactionManagementType.BEAN)
public class Playground {

    @PersistenceContext
    private EntityManager em;    
    @Resource
    private UserTransaction ut;

    private static Logger LOGGER = Logger.getLogger(Playground.class.getName());


    private void createQuestion(Quiz quiz, String qtext, String c1, Boolean b1, String c2, Boolean b2) {
        var q = new Question().text(qtext).quiz(quiz);
        em.persist(q);
        LOGGER.log(Level.FINE, "question serialization:" + q.getId());

        var mca = new MultipleChoiceAnswer().question(q); //.questionId(q.getId());
        em.persist(mca);
        LOGGER.log(Level.FINE, "mca serialization:" + mca.getId());
        q.multipleChoiceAnswer(mca);
        em.persist(q);
        LOGGER.log(Level.FINE, "question serialization:" + q.getId());

        em.persist(new MultipleChoiceItem().text(c1).isCorrect(b1).multipleChoiceAnswer(mca));
        em.persist(new MultipleChoiceItem().text(c2).isCorrect(b2).multipleChoiceAnswer(mca));
        //LOGGER.log(Level.FINE, "mcitem1 serialization:" + c1.getId());
        //LOGGER.log(Level.FINE, "mcitem2 serialization:" + c2.getId());
    }

    @PostConstruct
    void doSomeThings() {


        Runnable create = () -> {
            var ta = new TextAnswer().text("some answer");
            
            try {
                ut.begin();
                
                var quiz = new Quiz().name("Futurama").description("generic").userId(0L);
                em.persist(quiz);
                LOGGER.log(Level.FINE, "quiz serialization:" + quiz.getId());
                
                createQuestion(quiz,"What is Zoidbergs 1st name?", "John", true, "George", false);
                createQuestion(quiz,"What's Fry's PIN", "7710", false, "1077", true);


                quiz = new Quiz().name("Seinfeld").description("generic").userId(0L);
                em.persist(quiz);
                LOGGER.log(Level.FINE, "quiz serialization:" + quiz.getId());
                
                createQuestion(quiz,"King of your...", "Castle", false, "Domain", true);
                createQuestion(quiz,"What's making you thirsty?", "Double Dipped Chips", false, "These Pretzels", true);

                ut.commit();
            } catch (Exception e) {
                System.out.println("OOPS " + e);
                try {ut.rollback();} catch (Exception e2) {System.out.println("OOPS2 " + e2);}
            }
        };


        Runnable query = () -> {
            Query q = em.createQuery("select q from quiz q", Quiz.class);
            Quiz quiz = (Quiz)(q.getResultList().get(0));
            LOGGER.log(Level.FINE, "#Questions deserialized:" + quiz.getQuestions().size());
            LOGGER.log(Level.FINE, "1st Question text:" + quiz.getQuestions().get(0).getText());
            LOGGER.log(Level.FINE, "multichoice answer id:" + quiz.getQuestions().get(0).getMultipleChoiceAnswer().getId());
            LOGGER.log(Level.FINE, "1st multichoice text: " + quiz.getQuestions().get(0).getMultipleChoiceAnswer().getChoices().get(0).getText());
        };


        Runnable delete = () -> {   
            try {
                ut.begin();

                Query q1 = em.createQuery("DELETE FROM multipleChoiceItem");
                Query q2 = em.createQuery("DELETE FROM multipleChoiceAnswer");
                Query q3 = em.createQuery("DELETE FROM question");
                Query q4 = em.createQuery("DELETE FROM quiz");

                q2.executeUpdate();
                q3.executeUpdate();
                q4.executeUpdate();
                q1.executeUpdate();
            
                ut.commit();
            } catch (Exception e) {
                e.printStackTrace();
            }
        };


        //delete.run();
        //create.run();
        //query.run();
    }

}
