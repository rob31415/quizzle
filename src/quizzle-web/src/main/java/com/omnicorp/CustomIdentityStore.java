package com.omnicorp;

import java.util.Arrays;
import java.util.HashSet;

import javax.inject.Named;
import javax.security.enterprise.identitystore.*;
import javax.enterprise.context.ApplicationScoped;
import javax.security.enterprise.credential.UsernamePasswordCredential;

@Named
@ApplicationScoped
public class CustomIdentityStore implements IdentityStore {
    public CredentialValidationResult validate(UsernamePasswordCredential creds) {

        if(creds.compareTo("a","b")) {
            var rolesOfUser = new HashSet<>(Arrays.asList("admin","basic"));
            return new CredentialValidationResult("myuser", rolesOfUser);
        } else {
            //return IdentityStore.super.validate(creds);
            return CredentialValidationResult.INVALID_RESULT;
        }

    }
}

/*
java.io.IOException: java.io.IOException: ELY01177: Authorization failed.
	at org.wildfly.security.jakarta.authentication@1.17.1.Final//org.wildfly.security.auth.jaspi.impl.JaspiAuthenticationContext$1.handle(JaspiAuthenticationContext.java:111)

https://www.linkedin.com/learning/java-ee-7-web-services/absichern-eines-web-services?autoplay=true&resume=false
    container basiert
        add-user.sh und web.xml
    applikations baisert
    übergreifende standards WS-Security etc.
https://stackoverflow.com/questions/15541189/how-to-configure-simple-authentication-in-jboss7-1
https://access.redhat.com/documentation/en-us/red_hat_jboss_enterprise_application_platform/7.3/html-single/how_to_configure_identity_management/index
https://javaee.github.io/tutorial/security-api005.html#overview-of-the-custom-identity-store-example
*/