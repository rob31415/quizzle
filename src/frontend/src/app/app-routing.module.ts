import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import LandingComponent from './landing/landing.component'
//import { QuizSelectComponent } from './take-quiz/quiz-select/quiz-select.component';

const routes: Routes = [
  {path:"", component: LandingComponent},
  //{path:"select", component: QuizSelectComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
