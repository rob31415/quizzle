import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QuizSelectComponent } from './quiz-select/quiz-select.component';
import { ProgressDisplayComponent } from './progress-display/progress-display.component';
import { QuizQuestionComponent } from './quiz-question/quiz-question.component';
import { QuizResultComponent } from './quiz-result/quiz-result.component';
import { TakeQuizRoutingModule } from './take-quiz-routing.module';


@NgModule({
  declarations: [
    QuizSelectComponent,
    ProgressDisplayComponent,
    QuizQuestionComponent,
    QuizResultComponent
  ],
  imports: [
    CommonModule,
    TakeQuizRoutingModule
  ]
})
export class TakeQuizModule { }
