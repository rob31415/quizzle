import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {QuizSelectComponent} from "./quiz-select/quiz-select.component"
import {QuizQuestionComponent} from "./quiz-question/quiz-question.component"
import { QuizResultComponent } from './quiz-result/quiz-result.component';

const routes: Routes = [
  {
    path: 'take-quiz/select',
    component: QuizSelectComponent
  },
  {
    path: 'take-quiz/take',
    component: QuizQuestionComponent
  },
  {
    path: 'take-quiz/result',
    component: QuizResultComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class TakeQuizRoutingModule { }