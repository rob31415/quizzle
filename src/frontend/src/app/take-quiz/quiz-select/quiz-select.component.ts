import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-quiz-select',
  templateUrl: './quiz-select.component.html',
  styleUrls: ['./quiz-select.component.css']
})
export class QuizSelectComponent {
  constructor(private router: Router) {}

  take() {
    this.router.navigate(["take-quiz/take"])
  }
}
