import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HiscoreComponent } from './hiscore/hiscore.component'
import { LandingMenuComponent } from './landing-menu/landing-menu.component'
import LandingComponent from './landing.component';
import { RouterModule } from '@angular/router';


@NgModule({
  declarations: [
    HiscoreComponent,
    LandingMenuComponent,
    LandingComponent
  ],
  imports: [
    CommonModule,
    RouterModule
  ]
})
export class LandingModule { }
