import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-landing-menu',
  templateUrl: './landing-menu.component.html',
  styleUrls: ['./landing-menu.component.css']
})
export class LandingMenuComponent {
  constructor(private router: Router) {}

  take() {
    this.router.navigate(["take-quiz/select"])
  }
  logIn() {
    alert("LogIn")
  }
}
