# 5. Code organization

Date: 2021-06-07

## Status

Re-evaluating

## Context

Code (i.e. beans) has to be organized into several categories:

- maven projects
- servlets
- java modules
- java packages

Additionally, code for the API should be generated.

## Decision

|bean           |mvn module     |generated  |container  |servlet        |JPMS module|package|
---             | ---           | ---       | ---       | ---           | ---       | ---
|JSF/backing    |quizzle-web.war|no         |web        |FacesServlet   |view       |page|
|JAX-RS App     |quizzle-web.war|yes (*)    |web        |Application    |api        |system|
|JAX-RS IF      |quizzle-web.war|yes (*)    |web        |-              |api        |interface|
|JAX-RS IF impl |quizzle-web.jar|no         |ejb        |-              |api.impl   |interface.impl|
|DAOs           |quizzle-ejb.jar|no         |ejb        |-              |dao        |dao|
|models         |quizzle-ejb.jar|yes (*)    |ejb        |-              |api        |model|

notes: 

- maven modules follow the containers
- all maven modules are packed into "quizzle-web.ear"
- modules & packages are always prefixed with "com.omnicorp.quizzle"
- code generation is done at build time from OpenAPI .yaml file

The API code generation will be done via OpenAPI at build time via maven plugin.

### TODO

- introduce java modules (see above table)
- rethink if it's good or even possible to split a java module across multiple maven modules
- see also https://www.baeldung.com/maven-multi-module-project-java-jpms

## Consequences

### Regarding (*)

It's possible to control code generation to:

- work in both projects, quizzle-ejb and quizzle-web using the same .yaml file as input
- to generate in quizzle-ejb only the models (via "<generateApis>false" in pom.xml)
- to generate in quizzle-web only the api (via "<generateModels>false" in pom.xml)

Generated APIs depend on models at compile time.
quizzle-web depends on quizzle-ejb.
Since ejb is compiled first, this works.

### Mapping of generated models

The models are being generated from OpenAPI 2.0 via openapi-generator-maven-plugin (see "generate" goal in pom.xml).
Thus, the sourcecode isn't available for direct editing.
This means, annotation based mapping isn't an option, so we have to go with xml based mapping.

Note: The models are being used for JPA and for JSON simultaneously.

## Additional Notes

Differences From OpenAPI 2.0
OpenAPI 2.0 had separate sections for reusable components – definitions, parameters, responses and securityDefinitions. In OpenAPI 3.0, they all were moved inside components. Also, definitions were renamed to schemas and securityDefinitions were renamed to securitySchemes (note the different spelling: schemAs vs securitySchemEs). The references are changed accordingly to reflect the new structure:

OpenAPI 2.0                    OpenAPI 3.0
'#/definitions/User'         → '#/components/schemas/User'
'#/parameters/offsetParam'   → '#/components/parameters/offsetParam'
'#/responses/ErrorResponse'  → '#/components/responses/ErrorResponse'


What is the difference between Swagger Codegen and OpenAPI Generator?
Swagger Codegen is driven by SmartBear while OpenAPI Generator is driven by the community. More than 40 top contributors and template creators of Swagger Codegen have joined OpenAPI Generator as the founding team members.

